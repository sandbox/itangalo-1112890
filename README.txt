The URL ID module provides a field that gives an entity a 'global' ID, based
on the domain name, entity name and entity id (such as
http://example.com/node/1).
The idea is that remote entities can be downloaded, inheriting the remote URL ID
– a node could have the URL ID 'http://example2.com/node/123'.

The Lazy URL ID Reference module provides a lazy-loading reference field,
utilizing URL IDs. The idea is to refer to a URL ID such as
'http://example2.com/node/123'. If there is a local entity with this URL ID, the
reference field will point to it (eg. node/456), if not, it will point to the
remote source (eg. 'http://example2.com/node/123').
Furthermore, URL ID fields have *fetchers* – plugins that allow fetching remote
data and saving it locally. These fetchers can also affect the reference link,
for example by automatically download the referenced entity, or to redirect to
the remote source.

This is a work in progress. It is related to a project for being able to
download and sync help pages from drupal.org to your local site. See the
following links for more information:

* Specification/wish list for better help system: http://drupal.org/node/1095012
* Better help system for Drupal 8: http://drupal.org/node/1031972
* Want the ability to create multiple outlines/maps: http://drupal.org/node/995370
